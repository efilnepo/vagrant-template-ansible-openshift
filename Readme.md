Vagrantfile template for the Openshift
============================================================

Description
-----------

Template to provision Openshift 3.11 in the Virtualbox using Vagrant.

Usage
-----

```
git clone https://gitlab.com/common.vagrant/vagrant-openshift.git
cd vagrant-openshift
vagrant up
```

You are ready to start customization and using Vagrant.

Link for Openshift

https://172.28.128.13.xip.io:8443

Addons
------

```
curl https://gitlab.com/common.openshift.templates/openshift-addons/raw/master/kubevirt.sh | sh -

curl https://gitlab.com/common.openshift.templates/kubernetes-dashboard/raw/master/dashboard.sh | sh -
```